# README #


### What is this repository for? ###

This repository contains the code for the publication:

Tepole, A Buganza. "Computational systems mechanobiology of wound healing." Computer Methods in Applied Mechanics and Engineering 314 (2017): 46-70.
[Link to the paper](https://www.sciencedirect.com/science/article/pii/S0045782516302857)

### How do I get set up? ###

There is a Makefile with the basic recipes for compiling a sample simulation, which replicates the simulation corresponding to Figure 6 on the paper. You need a basic c++ compiler, basic c++ libraries. The only additional libraries that you need are [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) 


### Who do I talk to? ###

If you are interested in contributing to development or using of the code, please check out Prof. Buganza Tepole's [website](https://engineering.purdue.edu/tepolelab/)